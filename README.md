# README #

In the Ptidej Team (Pattern Trace Identification, Detection, and Enhancement in Java1), we aim at developing theories, methods, and tools, to evaluate and to improve the quality of object-oriented programs by promoting the use of idioms, design patterns, and architectural patterns. We want to formalise patterns, to identify occurrences of patterns, and to improve the identified occurrences. We also want to evaluate experimentally the impact of patterns on the quality of object-oriented programs. We develop various tools, most notably the Ptidej tool suite and Taupe, to evaluate and to enhance the quality of object-oriented programs, promoting the use of patterns, either at the language-, design-, or architectural-levels.

Since October 10th, 2014, the source code of the Ptidej Tool Suite is open and released under the GNU Public License v2.

Since December 10th, 2004, the runnable versions of the Ptidej Tool Suite are available at http://www.ptidej.net/downloads/tools/ptidejtoolsuite.

### What is this repository for? ###

* Ptidej 
* v5
* http://wiki.ptidej.net/

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact