/*
 * (c) Copyright 2001, 2002 Yann-Ga�l Gu�h�neuc,
 * Ecole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * Yann-Ga�l Gu�h�neuc, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR B PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN, ANY
 * LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF YANN-GAEL GUEHENEUC IS ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package ptidej.solver.test.java.fingerprint.comparison;

import padl.kernel.exception.ModelDeclarationException;
import ptidej.solver.Occurrence;
import ptidej.solver.fingerprint.Rule;

public final class CompositeJUnit extends Primitive {
	private static Occurrence[] BuiltSolutions;
	private static Occurrence[] BuiltSolutionsNoRule;

	public CompositeJUnit(final String name) {
		super(name);
	}
	protected void setUp()
		throws
			CloneNotSupportedException,
			IllegalAccessException,
			InstantiationException,
			ModelDeclarationException {
		Primitive.initialize("JUnit");

		if (CompositeJUnit.BuiltSolutions == null) {
			CompositeJUnit.BuiltSolutions =
				CompositeJUnit.automaticSolve(
					ptidej.solver.fingerprint.problem.CompositeMotif.class,
					"../JUnit v3.7/bin/",
					"JUnit",
					Rule.C_LEAF_ROLE_1);
		}
		if (CompositeJUnit.BuiltSolutionsNoRule == null) {
			CompositeJUnit.BuiltSolutionsNoRule =
				CompositeJUnit.automaticSolve(
					ptidej.solver.problem.CompositeMotif.class,
					"../JUnit v3.7/bin/",
					"JUnit");
		}
	}

	public void testNumberSolution() {
		CompositeJUnit.assertTrue(
			"Less solution with rules.",
			CompositeJUnit.BuiltSolutions.length
				< CompositeJUnit.BuiltSolutionsNoRule.length);
	}
}
