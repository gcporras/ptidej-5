/*
 * (c) Copyright 2001, 2002 Yann-Ga�l Gu�h�neuc,
 * Ecole des Mines de Nantes and Object Technology International, Inc.
 * 
 * Use and copying of this software and preparation of derivative works
 * based upon this software are permitted. Any copy of this software or
 * of any derivative work must include the above copyright notice of
 * Yann-Ga�l Gu�h�neuc, this paragraph and the one after it.
 * 
 * This software is made available AS IS, and THE AUTHOR DISCLAIMS
 * ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR B PARTICULAR
 * PURPOSE, AND NOT WITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN, ANY
 * LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF YANN-GAEL GUEHENEUC IS ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * All Rights Reserved.
 */
package ptidej.solver.test.java.fingerprint.comparison;

import junit.framework.TestCase;
import padl.creator.CompleteClassFileCreator;
import padl.kernel.IAbstractLevelModel;
import padl.kernel.ICodeLevelModel;
import padl.kernel.exception.CreationException;
import padl.kernel.exception.ModelDeclarationException;
import padl.kernel.impl.Factory;
import padl.util.repository.constituent.ConstituentRepository;
import padl.util.repository.file.DefaultFileRepository;
import ptidej.solver.fingerprint.ReducedDomainBuilder;
import ptidej.solver.fingerprint.Rule;
import util.PropertyManager;

public final class CompositeComposite2 extends TestCase {
	private static IAbstractLevelModel BuiltAbstractLevelModel;

	public CompositeComposite2(final String name) {
		super(name);
	}
	protected void setUp() throws CloneNotSupportedException,
			IllegalAccessException, InstantiationException,
			ModelDeclarationException {

		if (CompositeComposite2.BuiltAbstractLevelModel == null) {
			final ICodeLevelModel codeLevelModel =
				Factory.getInstance().createCodeLevelModel(
					"ptidej.example.composite2");
			try {
				codeLevelModel.create(new CompleteClassFileCreator(
					ConstituentRepository.getInstance(DefaultFileRepository
						.getInstance(ConstituentRepository.class)),
					new String[] { PropertyManager.getExamplesDirectory()
							+ "bin/ptidej/example/composite2/" }));
			}
			catch (final CreationException e) {
				e.printStackTrace();
			}
			final ReducedDomainBuilder rdg =
				new ReducedDomainBuilder(codeLevelModel);
			CompositeComposite2.BuiltAbstractLevelModel =
				rdg.computeReducedDomain(Rule.C_LEAF_ROLE_1);
		}
	}
	public void testNumberOfSolutions() {
		CompositeComposite2.assertEquals(
			"Number of entity",
			4,
			CompositeComposite2.BuiltAbstractLevelModel
				.getNumberOfConstituents());
	}
}
